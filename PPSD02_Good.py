# Description of this Program
# Author : Sarunya Nawwapornnimit
# Since : 2021-05-16
# Program Name : Read & Write File
# Program Language : Python
# Program Purpose : Write a small program

def read_index(str):                # reed file function
    filename = 'text.txt'           # value of text.txt
    f_object = open(filename,'r')   # give f_object to open text.txt and turn on read function
    print(f_object.read())          # print read function
    f_object.close()                # close file
    
def delete_index(str):              # delete file function
    filename = 'text.txt'           # value of text.txt
    f_object = open(filename,'w')   # value of text.txt use open file
    f_object.write("")              # write the blank into the file = delete file
    f_object.close                  # close file
    
def add_index(str):                                                 # add file function
    num_data = int(input("How many do you add to the file? : "))    # input the number of data to add the file give the valuename is num_data
    index = []                                                      # give the valuename : index is blank list and collect the data
    for indx in range(num_data):                                    # loop for to insert the data observe by number of data       
        insert_data = input("insert your data : ")
        index.append(insert_data)
    filename = 'text.txt'                                           # value of text.txt
    f_object = open(filename,'w')                                   # give f_object to open text.txt and turn on read function
    for N_data in index:                                            # loop for to write data observe by insert the data
        f_object.write(N_data + '\n') 
    f_object.close()                                                # close file
    
def accept_index(str):                                              # accept file function
     select1 = str(input("Are you accept this data? y/n : "))       # input Yes/No to accept data use value name : select1
     if select1 == 'y':                                             # if select 'y' do pass function
         pass
     elif select1 == 'n':                                           # but select 'n' do add file function
         print(add_index(str))
         
def  replace_index(str):                                                # replace file function                  
    num_data = int(input("How many do you replace to the file? : "))    # input the number of data to replace the file give the valuename is num_data
    index = []                                                          # give the valuename : index is blank list and collect the data
    for indx in range(num_data):                                        # loop for to insert the data observe by number of data  
        insert_data = input("insert your data : ")
        index.append(insert_data)
        
    insert_fname = input("Please write your file name : ")          # give the user insert filename to replace the file
    f_object = open(insert_fname+".txt",'w')                        # use the filename insert by user to open the write function
    for N_data in index:                                            # loop for to write data observe by insert the data
        f_object.write(N_data + '\n') 
    f_object.close()                                                # close file
    
print(read_index(str))                                              # do read file function
print("1 Delete")                                                   
print("2 Add")
print("3 Accept")
print("4 Replace")
choose1 = int(input("Select the function : "))                      # give user to select the function value name : choose1
if choose1 == 1:                                                    # select number 1
    print(delete_index(str))                                        # do delete file function
elif choose1 == 2:                                                  # select number 2
    print(add_index(str))                                           # do add file function
elif choose1 == 3:                                                  # select number 3
    print(accept_index(str))                                        # do accept file function
elif choose1 == 4:                                                  # select number 4
    print(replace_index(str))                                       # do replace file function
